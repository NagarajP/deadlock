package com.myzee.deadlock;

public class TestDeadLock {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestDeadLock t = new TestDeadLock();
		t.t1.start();
		t.t2.start();
	}
		String str1 = "Java";
		String str2 = "Unix";
		
		Thread t1 = new Thread("Thread1") {
			@Override
			public void run() {
				// TODO Auto-generated method stub
//				super.run();
				synchronized (str1) {
					synchronized(str2) {
						System.out.println(str1 + str2);
					}
				}
			}
		};
		
		Thread t2 = new Thread ("Thread2") {
			@Override
			public void run() {
				// TODO Auto-generated method stub
//				super.run();
				synchronized (str2) {
					synchronized (str1) {
						System.out.println(str2 + str1);
					}
				}
			}
		};
}
