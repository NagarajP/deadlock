package com.myzee.deadlock;

public class DeadLockUsinMethods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A a = new A();
		B b = new B();
		MyThreadA t1 = new MyThreadA(a, b);
		MyThreadB t2 = new MyThreadB(a, b);
		
		t1.start();
		t2.start();
	}

}

class MyThreadA extends Thread{
	A a;
	B b;
	MyThreadA(A a, B b) {
		this.a = a;
		this.b = b;
	}
	public void run() {
		a.m1(b);
	}
}

class MyThreadB extends Thread{
	A a;
	B b;
	MyThreadB(A a, B b) {
		this.a = a;
		this.b = b;
	}
	public void run() {
		b.m2(a);
	}
}

class A {
	public synchronized void m1(B b) {
		System.out.println("Calling a's m1()");
		b.last();
	}

	public synchronized void last() {
		System.out.println("Calling a's last()");
		// TODO Auto-generated method stub
		
	}
}

class B {
	public synchronized void m2(A a) {
		System.out.println("Calling b's m2()");
		a.last();
	}

	public synchronized void last() {
		System.out.println("Calling b's last()");
		// TODO Auto-generated method stub
		
	}
}